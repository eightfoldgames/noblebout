//
//  SKLabel+Animations.swift
//  NobleBout
//
//  Created by ハローダヴィス on 6/24/19.
//  Copyright © 2019 ハローダヴィス. All rights reserved.
//

import Foundation
import SpriteKit


extension SKLabelNode {
    func countDown(counter: Int, statusArr: [String], _ completion: @escaping()->()) {
        self.text = statusArr[counter - counter]
        
        fadInAndOut(count: counter, index: 1, statusArr: statusArr, duration: 0.6) {
            completion()
        }
    }
    
    
    func fadInAndOut(count: Int, index: Int, statusArr: [String], duration: TimeInterval, _ completion: @escaping()->()) {
        let isLastFO = (count <= 1) ? true : false
        
        self.run(.fadeIn(withDuration: duration)) {
            self.run(.fadeOut(withDuration: duration)) {
                if isLastFO {
                    completion()
                }
                else {
                    self.text = statusArr[index]
                    self.fadInAndOut(count: count - 1, index: index + 1, statusArr: statusArr, duration: duration) {
                        completion()
                    }
                }
            }
        }
    }
}
