//
//  StringsLibrary.swift
//  NobleBout
//
//  Created by ハローダヴィス on 6/25/19.
//  Copyright © 2019 ハローダヴィス. All rights reserved.
//

import Foundation


/// Objects(SKNodes) from the scene.
struct StageObjects {
    static let background = "stage"
    static let playerOne = "PlayerOne"
    static let playerTwo = "PlayerTwo"
    
    static let playerOneIdleArea = "p1i"
    static let playerTwoIdleArea = "p2i"
    
    static let playerOneAttackArea = "p1a"
    static let playerTwoAttackArea = "p2a"
    
    static let punchButton = "p"
    static let kickButton = "k"
    static let abilityButton = "a"
    static let spiritButton = "s"
    
    static let matchNotes = "MatchNotes"
    
    static let timer = "timer"
}


