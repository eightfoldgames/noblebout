//
//  StageScene.swift
//  NobleBout
//
//  Created by ハローダヴィス on 4/28/19.
//  Copyright © 2019 Lee Dav. All rights reserved.
//

import Foundation
import SpriteKit

fileprivate let countDown: [String] =  ["1", "2", "3", "GO!"]
typealias Method = ()->()

class StageScene: SKScene {
    var gsController = GameStateController.sharedInstance
    
    enum MatchNote {
        case point
        case strike
        case boutWinner
        case draw
    }
    
    var pauseMenu = SKLabelNode()
    
    var trainingModeActive: Bool = false
    var isListeningToPlayer: Bool = false
    var matchShouldRestart: Bool = false

    // Players
    var playerOne: Player!
    var playerTwo: Player!

    var playerOneSprite = SKSpriteNode()
    var playerTwoSprite = SKSpriteNode()
    
    
    // attatck sprites
    var playerOneSpriteA = SKSpriteNode()
    var playerTwoSpriteA = SKSpriteNode()
    
    // Temp controls
    var pButton = SKSpriteNode()
    var kButton = SKSpriteNode()
    var aButton = SKSpriteNode()

    
    var playerInterface: PlayerInterface =  PlayerInterface()
    var NPCInterface: PlayerInterface =  PlayerInterface()
    var matchNotes: SKLabelNode = SKLabelNode()
    var challengeScreen: ChallengeScreen = ChallengeScreen()
    var stageBackground: SKSpriteNode = SKSpriteNode()
    
    // Timer Stuff
    var timerDefaultPosition: CGPoint = CGPoint(x: -700.7,y: -165.598)
    var timerCompletionPosition: CGPoint = CGPoint(x: -49.049, y: -165.598)
    var timerSprite = SKSpriteNode()
    
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        self.camera?.addChild(challengeScreen.mainNode)
        challengeScreen.setUp(p1: playerOne.hero, p2: playerTwo.hero, bgSize: CGSize(width: 750, height: 1334)) {
            self.challengeScreen.mainNode.run(.fadeOut(withDuration: 0.3)) {
                self.challengeScreen.mainNode.removeFromParent()
                self.loadScene()
            }
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            if let camera = self.camera {
                let location = t.location(in: camera)
                
                if gsController.bout!.boutActive {
                    if pButton.contains(location) {
                        playerOne.playerChoice = .punch
                    }
                    else if kButton.contains(location) {
                        playerOne.playerChoice = .kick
                    }
                    else if aButton.contains(location) {
                        playerOne.playerChoice = .ability
                    }
                }
            }
        }
    }
    
    func loadScene() {
        gsController.bout = Bout(p1: playerOne, p2: Player(hero: gsController.currentApponent))
        
        self.camera?.children.forEach { (child) in
            if child.name == StageObjects.background {
                stageBackground = child as! SKSpriteNode
                stageBackground.texture = playerTwo.hero.stageImage
            }
            if child.name == StageObjects.playerOne {
                playerInterface = PlayerInterface(node: child)
                
            }
            if child.name == StageObjects.playerTwo {
                NPCInterface = PlayerInterface(node: child)
            }
            if child.name == StageObjects.playerOneIdleArea {
                playerOneSprite = child as! SKSpriteNode
                playerOneSprite.texture = playerOne.hero.image
                playerOne.run(action: .idle, for: playerOneSprite) {}
            }
            else if child.name == StageObjects.playerTwoIdleArea {
                playerTwoSprite  = child as! SKSpriteNode
                playerTwoSprite.texture = gsController.currentApponent.image
                
                let a = SKAction.repeatForever(SKAction(named: gsController.currentApponent.rawValue + Action.idle.rawValue, duration: 1.5) ?? SKAction())
                playerTwoSprite.run(a)
                // playerOne.run(action: .idle, for: playerTwoSprite)
                
            }
            else if child.name == StageObjects.timer {
                timerSprite = child as! SKSpriteNode
            }
            else if child.name == StageObjects.playerOneAttackArea {
                playerOneSpriteA = child as! SKSpriteNode
            }
            else if child.name == StageObjects.playerTwoAttackArea {
                playerTwoSpriteA = child as! SKSpriteNode
            }
            else if child.name == StageObjects.punchButton {
                pButton = child as! SKSpriteNode
            }
            else if child.name == StageObjects.kickButton {
                kButton = child as! SKSpriteNode
            }
            else if child.name == StageObjects.abilityButton {
                aButton = child as! SKSpriteNode
            }
            if child.name == StageObjects.matchNotes {
                matchNotes = child as! SKLabelNode
            }
        }
        
        startMatch()
    }
    
    
    // RunMatch
    func startMatch() {
        self.matchNotes.fontSize = 100
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.matchNotes.countDown(counter: countDown.count, statusArr: countDown) {
                self.isListeningToPlayer = true
                self.startRound {
                    self.runBout() {
                        //                                                self.gsController.bout?.setInactive()
                        print("Choice should be made.")
                        
                    }
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                // display What the NPS could have chosen *If
            }
        }
    }
    
    
    func startRound(completion: Method? = nil) {
        // (x: -700.7,y: -165.598)
        guard let bout = gsController.bout else {
            return
        }
        
        bout.NPCChooser()
        
        timerSprite.alpha = 1
        timerSprite.run(.move(to: timerCompletionPosition, duration: 3)) {
            bout.runBout {
                self.resetTimer()
                completion?()
            }
        }
        // end (x: -49.049, y: -165.598)
    }
    
    
    func resetTimer() {
        timerSprite.run(.fadeAlpha(by: 0, duration: 0.5))
        timerSprite.run(.move(to: timerDefaultPosition, duration: 0))
    }
    
    
    func runBout(completion: Method? = nil) {
        guard let bout = self.gsController.bout else {
            return
        }
        
        if bout.roundShouldEnd() || bout.matchComplete {
            resetBout(bout: bout)
            bout.resetBout(shouldResetStats: true)
        }
        else {
            self.startRound {
                self.runWinnerActions()
            }
        }
    }
    
    
    func endMatch() {
//        let apponent = gsController.apponents[0]
        
    }
    
    
    func announceMatchPoint(with name: String = "", noteType: MatchNote, completion: Method? = nil) {
        var matchNote: String
        
        switch noteType {
        case .strike:
            matchNote = "\(name) made a strike!"
        case .point:
            self.matchNotes.fontSize = 22
             matchNote = "Match point goes to \(name)!"
        case .boutWinner:
            matchNote = "\(name) is the Winner!"
        case .draw:
            matchNote = "DRAW!"
        }
        
        self.matchNotes.text = matchNote
        self.matchNotes.run(.fadeIn(withDuration: 1)) {
            self.matchNotes.run(.fadeOut(withDuration: 0.6))
            completion?()
        }
    }
    
    
    func takeDamageAnim(playerType: Bout.PlayerType) {
        let td = SKAction(named: Action.takeDamage.rawValue) ?? SKAction()
        
        switch playerType {
        case .playerOne:
            playerOneSprite.run(td)
        default:
            playerTwoSprite.run(td)
        }
    }
    
    
    func resetBout(bout: Bout) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            bout.resetBout()
            self.startRound {
                self.runBout {
                    
                }
            }
        }
    }
    
    
    func winnerActions(bout: Bout, interface: PlayerInterface, player: Player, sprite: SKSpriteNode, type: Bout.PlayerType) {
        self.matchNotes.fontSize = 11
        self.announceMatchPoint(with: player.hero.name, noteType: .strike)
        
        sprite.alpha = 0
        player.run(action: player.playerChoice, for: playerOneSpriteA) {
            var hp: Int = 0
    
            switch type {
            case .playerOne:
                hp = bout.playerTwo.baseStats.hp
                self.takeDamageAnim(playerType: .playerTwo)
            case .playerTwo:
                hp = bout.playerOne.baseStats.hp
                self.takeDamageAnim(playerType: .playerOne)
            default:
                break
            }
            
            if hp <= 0 {
                interface.hp.text = "\(0)"
                self.announceMatchPoint(with: player.hero.name, noteType: .point) {
                    bout.awardPoint(to: type)
                }
            }
            else {
                interface.hp.text = "\(hp)"
            }
            
            sprite.alpha = 1
        }
    }
    
    
    func runWinnerActions() {
        guard let bout = self.gsController.bout else {
            return
        }
        
        if bout.boutWinner == .playerOne {
            self.winnerActions(bout: bout, interface: self.playerInterface, player: self.playerOne, sprite: self.playerOneSprite, type: .playerOne)
        }
        else if bout.boutWinner == .playerTwo {
            self.winnerActions(bout: bout, interface: self.NPCInterface, player: self.playerTwo, sprite: self.playerTwoSprite, type: .playerTwo)
        }
        else {
            self.draw()
        }
        
        self.startRound()
    }
    
    
    func draw() {
        DispatchQueue.main.async {
            self.playerTwo.run(action: self.playerTwo.playerChoice, for: self.playerOneSpriteA) {
                self.playerOneSprite.alpha = 1
            }
            self.playerOne.run(action: self.playerOne.playerChoice, for: self.playerTwoSpriteA) {
                self.playerTwoSprite.alpha = 1
            }
        }
        
        announceMatchPoint(noteType: .draw)
    }
}
