//
//  TitleScreen.swift
//  NobleBout
//
//  Created by ハローダヴィス on 6/14/19.
//  Copyright © 2019 ハローダヴィス. All rights reserved.
//

import Foundation
import SpriteKit

class TitleScreen: SKScene {
    var flameEmmiter: SKEmitterNode = SKEmitterNode()
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        flameEmmiter = getPartical(name: "Flames")
        
        if let emitter = self.childNode(withName: "test") as? SKEmitterNode {
            self.addChild(flameEmmiter)
            flameEmmiter.position = emitter.position
        }
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let scene = SelectScreen(fileNamed: "SelectScreen")  else {
            return
        }
        
        scene.scaleMode = .aspectFill
        self.view?.presentScene(scene, transition: SKTransition.fade(withDuration: 0.4))
    }
}


public func getPartical(name: String) -> SKEmitterNode {
    if let partical = SKEmitterNode(fileNamed: name) {
        return partical
    }
    else {
        return SKEmitterNode()
    }
}
