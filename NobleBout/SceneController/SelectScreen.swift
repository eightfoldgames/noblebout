//
//  SelectScreen.swift
//  NobleBout
//
//  Created by Lee Dav on 4/27/19.
//  Copyright © 2019 Lee Dav. All rights reserved.
//

import SpriteKit

class SelectScreen: SKScene {
    var gsController = GameStateController.sharedInstance
    
    var selectedCharecterIcon: SKSpriteNode = SKSpriteNode()
    var nameLbl: SKLabelNode = SKLabelNode()
    var selectedCharecter: Hero = .masa
    var selectButton: SKLabelNode = SKLabelNode()
    
    
    var charecterDict: [Hero: SKSpriteNode] = [:]

    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        gsController.setApponents()
        
        self.camera?.children.forEach { child in
            if child.name == "charecter" {
                selectedCharecterIcon = child as! SKSpriteNode
            }
            
            if child.name == "select" {
                selectButton = child as! SKLabelNode
            }
            
            if child.name == "name" {
                nameLbl = child as! SKLabelNode
            }
            
            if let child = child as? SKSpriteNode,
                let name = child.name {
                print(name)
                for hero in heros {
                    if hero.checkIsHero(name: name) {
                        print(name)
                        charecterDict[hero] = child
                    }
                }
            }
        }
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            if let camera = self.camera {
                let location = t.location(in: camera)
                
                if selectButton.contains(location) {
                    guard let scene = StageScene(fileNamed: "Stage")  else {
                        return
                    }
                    
                    scene.playerOne = Player(hero: selectedCharecter)
                    
                    let apponent = Hero.fragarach // gsController.apponents[0]
                    
                    scene.playerTwo = Player(hero: apponent)
                    gsController.currentApponent = apponent
                    gsController.apponents.removeAll { (hero) -> Bool in
                        if hero == apponent {
                            return true
                        }
                        else {
                            return false
                        }
                    }
                    scene.scaleMode = .aspectFill
                    if let view = self.view {
                        view.presentScene(scene, transition: SKTransition.fade(withDuration: 0.4))
                    }
                }
                
                // TODO: - this adds charecter selection functionalitysys
                charecterDict.forEach { (hero, sprite) in
                    print(hero.rawValue)
                    if sprite.contains(location) {
                        selectedCharecter = hero
                        print(hero.rawValue + "1")
                        self.nameLbl.text = self.selectedCharecter.rawValue
                        self.selectedCharecterIcon.texture = hero.icon
                        self.gsController.apponents = hero.apponents
                    }
                }
            }
        }
    }
}


extension Array where Element == Int {
    func specialOrderRange() {
    }
}
