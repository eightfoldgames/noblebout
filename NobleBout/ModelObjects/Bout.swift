//
//  Bout.swift
//  NobleBout
//
//  Created by ハローダヴィス on 5/1/19.
//  Copyright © 2019 Lee Dav. All rights reserved.
//

import Foundation
import SpriteKit

class Bout {
    enum PlayerType {
        case playerOne
        case playerTwo
        case neither
    }
    
    var playerOne: Player
    var playerTwo: Player
    
    var wins1: [Int] = []
    var wins2: [Int] = []
    
    var boutWinner: PlayerType = .neither
    var boutActive: Bool = false
    var matchComplete: Bool = false
    
    let choices: [Action] = [.punch, .kick, .ability]
    
    init(p1: Player, p2: Player) {
        self.playerOne = p1
        self.playerTwo = p2
    }
    
    
    func runBout(completion: @escaping()->()) {
        boutActive = true
        
        compareChoices()
        checkWins()
        
        completion()
    }
    
    
    func setInactive() {
        boutActive = false
    }
    
    
    func NPCChooser() {
        let randomNumber = arc4random_uniform(UInt32(choices.count))
        playerTwo.playerChoice = choices[Int(randomNumber)]
    }
    
    
    private func compareChoices() {
        let player = compare(choice1: playerOne.playerChoice, choice2: playerTwo.playerChoice)
        boutWinner = player
    }
    
    
    func awardPoint(to player: PlayerType) {
        if player == .playerOne {
            wins1.append(1)
        }
        else {
            wins2.append(1)
        }
    }
    
    
    private func compare(choice1: Action, choice2: Action) -> PlayerType {
        if playerOne.playerChoice != .none {
            if (choice1 == .punch && choice2 == .kick) {
                playerTwo.recieveDamage(with: choice1, from: playerOne)
                
                return .playerOne
            }
            else if (choice1 == .kick && choice2 == .punch) {
                playerOne.recieveDamage(with: choice2, from: playerTwo)
                
                return .playerTwo
            }
            if (choice1 == .punch && choice2 == .ability) {
                playerOne.recieveDamage(with: choice2, from: playerTwo)

                return .playerTwo
            }
            else if (choice1 == .ability && choice2 == .punch) {
                playerTwo.recieveDamage(with: choice1, from: playerOne)
                return .playerOne
            }
            if (choice1 == .ability && choice2 == .kick) {
                playerOne.recieveDamage(with: choice2, from: playerTwo)

                return .playerTwo
            }
            else if (choice1 == .kick && choice2 == .ability) {
                playerTwo.recieveDamage(with: choice1, from: playerOne)

                return .playerOne
            }
        }
        else {
            playerOne.recieveDamage(with: choice2, from: playerTwo)
        }
        
        return .playerTwo
    }
    
    
    private func checkWins() {
        if wins1.count >= 2 {
            boutWinner = .playerOne
            matchComplete = true
        }
        else if wins2.count >= 2{
            boutWinner = .playerTwo
            matchComplete = true
        }
    }
    
    
    func resetBout(shouldResetStats: Bool = false) {
        checkWins()
        setInactive()
        
        boutWinner = .neither
        
        playerOne.playerChoice = .none
        playerTwo.playerChoice = .none
        
        if shouldResetStats {
            playerOne.resetStats()
            playerTwo.resetStats()
        }
    }
    
    func roundShouldEnd() -> Bool {
        if playerOne.baseStats.hp == 0 || playerTwo.baseStats.hp == 0 {
            return true
        }
        else {
            return false
        }
    }
}
