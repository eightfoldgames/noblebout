//
//  Player.swift
//  NobleBout
//
//  Created by ハローダヴィス on 4/30/19.
//  Copyright © 2019 Lee Dav. All rights reserved.
//

import Foundation
import SpriteKit


class PlayerInterface {
    var mainNode: SKNode = SKNode()
    var hp: SKLabelNode = SKLabelNode()
    var tp: SKLabelNode = SKLabelNode()
    
    var s1: SKEmitterNode = SKEmitterNode()
    var s2: SKEmitterNode = SKEmitterNode()
    var s3: SKEmitterNode = SKEmitterNode()
    var s4: SKEmitterNode = SKEmitterNode()
    
    var powerUps: [SKEmitterNode: Bool] = [:]
    
    
    init() {
        
    }
    
    convenience init(node: SKNode) {
        self.init()
        node.children.forEach { child in
            if child.name == "hp" {
                self.hp = child as! SKLabelNode
            }
            else if child.name == "tp" {
                self.tp = child as! SKLabelNode
            }
            else if child.name == "one" {
                self.s1 = getPartical(name: "SuperEnergy")
                self.s1.position = child.position
                self.s1.alpha = 1
            }
            else if child.name == "two" {
                self.s2 = getPartical(name: "SuperEnergy")
                self.s2.position = child.position
                self.s2.alpha = 0
            }
            else if child.name == "three" {
                self.s3 = getPartical(name: "SuperEnergy")
                self.s3.position = child.position
                self.s3.alpha = 0
            }
            else if child.name == "four" {
                self.s4 = getPartical(name: "SuperEnergy")
                self.s4.position = child.position
                self.s4.alpha = 0
            }
        }
        
        mainNode = node
        
        powerUps = [
            s1:false,
            s2:false,
            s3:false,
            s4:false]
        
        mainNode.addChild(s1)
//        powerUp()
    }

    
    func powerUp() {
        for value in powerUps.enumerated() {
            mainNode.addChild(value.element.key)
            if (value.element.value == false) {
                // set partical with key
                value.element.key.alpha = 1
                print("Level: \(value.offset) achieved!")
                self.powerUps[value.element.key] = true
                //test
//                return
            }
        }
        
        // check if all power ups are hit
//        if (powerUps.contains { (key, value) -> Bool in return (value == false) }) {}
    }
}

class Player {
    enum State {
        case idle
        case burning
        case stunned
        case victory
        case ko
    }
    
    var hero: Hero = .masa
    var baseStats = BaseStats()
    var currentState: State = .idle
    var playerChoice: Action = .none
    
    var isDefeated: Bool = false
    
    init(hero: Hero) {
        self.hero = hero
    }
    
    func run(action: Action, for sprite: SKSpriteNode, with cpuSprite: SKSpriteNode? = nil, completion: @escaping ()->()) {
        if action == .idle {
            let a = SKAction.repeatForever(SKAction(named: hero.rawValue + action.rawValue, duration: 1.5) ?? SKAction())
            sprite.run(a)
        }
        else {
            let a = SKAction(named: hero.rawValue + action.rawValue) ?? SKAction()
            sprite.texture = hero.image
            sprite.alpha = 1
           
            DispatchQueue.main.async {
                sprite.run(a) {
                    sprite.alpha = 0
                    completion()
                }
            }
        }
    }
    
    
    func resetStats() {
        baseStats.hp = 100
        baseStats.tp = 60
    }
    
    
    func recieveDamage(with action: Action, from player: Player) {
        switch action {
        case .punch:
            baseStats.hp = baseStats.hp - 20
        case .kick:
            baseStats.hp = baseStats.hp - 10

        case .ability:
            baseStats.hp = baseStats.hp - 20
//            if player.hero == .masa {
//            }
        case .special:
            print("no damage yet")
        default:
            print("no damage")
        }
        
        if baseStats.hp <= 0 {
            isDefeated = true
        }
    }
}
