//
//  ChallengeScreen.swift
//  NobleBout
//
//  Created by Anthony Torres on 6/29/19.
//  Copyright © 2019 ハローダヴィス. All rights reserved.
//

import Foundation
import SpriteKit

struct ChallengeScreen {
    var mainNode: SKNode = SKNode()
    var player1: SKSpriteNode = SKSpriteNode()
    var player2: SKSpriteNode = SKSpriteNode()
    var bg: SKSpriteNode = SKSpriteNode()
    var versusNode: SKNode = SKNode()
    var v: SKLabelNode = SKLabelNode(text: "V")
    var s: SKLabelNode = SKLabelNode(text: "s")
    
    var versusNodeArray: [SKLabelNode] {
        return [v, s]
    }
    var mainNodeArray: [SKSpriteNode] {
        return [player1, player2, bg]
    }
    
    
    
    mutating func setUp(p1: Hero, p2: Hero, bgSize: CGSize, completion: @escaping()->()) {
        let playerSize: CGSize = CGSize(width: 650, height: 650)
        let yValue: Int = 250
        let pixelFont: String = "ARCADEPI"
        
        player1 = SKSpriteNode(texture: p1.icon, size: playerSize)
        player1.position = CGPoint(x: -710, y: yValue)
        
        player2 = SKSpriteNode(texture: p2.icon, size: playerSize)
        player2.position = CGPoint(x: 625, y: yValue)
        
        bg = SKSpriteNode(color: .black, size: bgSize)
        
        
        mainNodeArray.forEach { (node) in
            self.mainNode.addChild(node)
        }
        versusNodeArray.forEach { (node) in
            node.fontName = pixelFont
            self.versusNode.addChild(node)
        }
        
        mainNode.addChild(versusNode)
        mainNode.zPosition = 10
        bg.zPosition = 0
        player1.zPosition = 1
        player2.zPosition = 1
        versusNode.zPosition = 2
        versusNode.alpha = 0
        
        animateScreen() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                completion()
            }
        }
    }
    
    
    func animateScreen(completion: @escaping()->()) {
        player1.run(.moveTo(x: -244.128, duration: 0.7)) {
           self.player2.run(.moveTo(x: 242.871, duration: 0.7)) {
            self.versusNode.run(.fadeIn(withDuration: 0.4))
            completion()
            }
        }
        
    }
}

