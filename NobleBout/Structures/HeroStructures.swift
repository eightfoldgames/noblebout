//
//  Hero.swift
//  NobleBout
//
//  Created by Lee Dav on 4/28/19.
//  Copyright © 2019 Lee Dav. All rights reserved.
//

import Foundation
import SpriteKit

var heros: [Hero] = [.masa, .tetsu, .eris, .griff, .abziu, .fragarach]


enum Hero: String {
    case masa  = "Masa"
    case tetsu = "Tetsu"
    case eris  = "Eris"
    case griff = "Giff"
    case abziu = "Abziu"
    case fragarach = "Fragarach"
    
    var name: String {
        return self.rawValue
    }
    
    var icon: SKTexture {
        return SKTexture(imageNamed: self.rawValue)
    }
    
    var image: SKTexture {
        return SKTexture(imageNamed: self.rawValue + "C")
    }

    var stageImage: SKTexture {
        return SKTexture(imageNamed: self.rawValue + "Stage")
    }
    
    var apponents: [Hero] {
        var order: [Hero]
        
        switch self {
        case .masa:
            order = [.griff, .eris, .abziu, .tetsu, .fragarach]
        case .tetsu:
            order = [.eris, .griff, .abziu, .masa, .fragarach]
        case .eris:
            order = [.masa, .abziu, .tetsu, .griff, .fragarach]
        case .griff:
            order = [.eris, .tetsu, .masa, .abziu, .fragarach]
        case .abziu:
            order = [.eris, .masa, .tetsu, .griff, .fragarach]
        case .fragarach:
            order = [.griff, .abziu, .tetsu, .eris, .masa]
        }
        
        return order
    }

    
    func getObjectInScene(scene: SKScene) -> SKSpriteNode? {
        return scene.camera?.childNode(withName: self.rawValue.lowercased()) as? SKSpriteNode ?? SKSpriteNode()
    }
    
    func checkIsHero(name: String) -> Bool {
        var answer: Bool = false
        
        heros.forEach { hero in
            if self.rawValue == name || self.rawValue.lowercased() == name {
                answer = true
            }
            else {
                answer = false
            }
        }
        return answer
    }
}


struct BaseStats {
    enum SpiritLevel {
        case zero
        case one
        case two
        case three
        case four
    }
    
    var hp: Int = 100
    var tp: Int = 50
    var spiritLevel: SpiritLevel = .zero
}

enum Action: String {
    case idle = "Idle"
    case punch = "Punch"
    case kick = "Kick"
    case ability = "Ability"
    case special = "Special+"
    case takeDamage = "TakeDamage"
    case none = "none"
}
