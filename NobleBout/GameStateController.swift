//
//  GameStateController.swift
//  NobleBout
//
//  Created by ハローダヴィス on 4/28/19.
//  Copyright © 2019 Lee Dav. All rights reserved.
//

import Foundation
import SpriteKit

class GameStateController {
    static var sharedInstance = GameStateController()
    
    enum Difficulty: Float {
        case easy = 3
        case normal = 2
        case hard = 0.5
    }
    
    
    var finalBossUnlocked: Bool = false
    var difficulty: Difficulty = .normal
    var chosenPlayer: Hero = .masa
    var currentApponent: Hero = .tetsu
    var bout: Bout?
    
    var apponents: [Hero] = []
    
    func setMatch() {
        apponents.removeAll { (hero) -> Bool in
            if hero == currentApponent {
                return true
            }
            else {
                return false
            }
        }
    }
    
    
    func resetGame(with view: SKView?) {
        guard let scene = TitleScreen(fileNamed: "TitleScreen") else {
            return
        }
        
        scene.scaleMode = .aspectFill
        view?.presentScene(scene, transition: SKTransition.fade(withDuration: 0.4))
    }
    
    
    func setApponents() {
        chosenPlayer.apponents.forEach { (hero) in
            self.apponents.append(hero)
        }
    }
}
